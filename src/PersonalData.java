
public class PersonalData {
	private String name, surname;
	private Adres adres;

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public Adres getAdres() {
		return adres;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setAdres(Adres adres) {
		this.adres = adres;
	}

}
