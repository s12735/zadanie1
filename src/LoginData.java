public class LoginData {
	private String login, password;
	private Permissions permissions;
	private PersonalData perdonalData;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Permissions getPermissions() {
		return permissions;
	}

	public void setPermissions(Permissions permissions) {
		this.permissions = permissions;
	}

	public PersonalData getPerdonalData() {
		return perdonalData;
	}

	public void setPerdonalData(PersonalData perdonalData) {
		this.perdonalData = perdonalData;
	}

}
